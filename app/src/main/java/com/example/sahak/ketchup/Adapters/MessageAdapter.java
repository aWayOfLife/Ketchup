package com.example.sahak.ketchup.Adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sahak.ketchup.MainActivity;
import com.example.sahak.ketchup.POJO.MessagePojo;
import com.example.sahak.ketchup.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

/**
 * Created by sahak on 05-04-2018.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private ArrayList<MessagePojo> messageList;
    public static class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView textSender, textTime, textMessage;
        public CircularImageView textSenderImage;
        public MessageViewHolder(View view) {
            super(view);
            textSender = (TextView) view.findViewById(R.id.text_sender);
            textTime = (TextView) view.findViewById(R.id.text_time);
            textMessage = (TextView) view.findViewById(R.id.text_message);
            textSenderImage = view.findViewById(R.id.text_sender_image);
        }
    }


    public MessageAdapter(ArrayList<MessagePojo> messageList) {
        this.messageList = messageList;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);

        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        MessagePojo messagePojo = messageList.get(position);
        holder.textSender.setText(messagePojo.getText_sender());
        holder.textTime.setText(messagePojo.getText_time());
        holder.textMessage.setText(messagePojo.getText_message());
        if(messagePojo.getText_sender().toString().equals("Kingshuk"))
        {
            holder.textSenderImage.setImageResource( R.drawable.kingshuk);
        }
        else
        {
            holder.textSenderImage.setImageResource( R.drawable.richa);
        }

    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }
}


