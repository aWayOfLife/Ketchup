package com.example.sahak.ketchup.Fragments;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sahak.ketchup.Adapters.MessageAdapter;
import com.example.sahak.ketchup.MainActivity;
import com.example.sahak.ketchup.POJO.MessagePojo;
import com.example.sahak.ketchup.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class MessageFragment extends Fragment {

    RecyclerView messageRecyclerView;
    MessageAdapter messageAdapter;
    ArrayList<MessagePojo> messageList;
    LinearLayoutManager mLayoutManager;
    TextView activeMonth;
    String month[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
    int maxDays[]={31,28,31,30,31,30,31,31,30,31,30,31};

    public MessageFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_message, container, false);
        activeMonth= rootView.findViewById(R.id.active_month);
        prepareEntryData();
        messageRecyclerView = (RecyclerView) rootView.findViewById(R.id.message_recylcer_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        messageRecyclerView.setLayoutManager(mLayoutManager);



        return  rootView;

    }

    public void prepareEntryData()  {
        messageList = new ArrayList<MessagePojo>();

        MyFileReader myFileReader = new MyFileReader();
        myFileReader.execute("chat");
    }





    class MyFileReader extends AsyncTask<String, ArrayList<MessagePojo>, ArrayList<MessagePojo>> {


        @Override
            protected ArrayList<MessagePojo> doInBackground(String... params) {
                ArrayList<MessagePojo> unOrganizedList = new ArrayList<MessagePojo>();
                String line = null, current=null; int count = 0;
                MessagePojo c=null;
                try {
                    InputStream file = getContext().getAssets().open(params[0].replaceAll("'", "") + ".txt");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(file));

                    while ((line = reader.readLine()) != null) {

                        current=line;
                        if(current.indexOf(':')==-1 || current.indexOf(':')>15)
                        {
                            MessagePojo repeatPojo = unOrganizedList.get(unOrganizedList.size()-1);
                            repeatPojo.setText_message(repeatPojo.getText_message()+"\n\n"+ current);
                        }
                        else {
                            c = pojoFromLine(current);
                            if (c != null)
                                unOrganizedList.add(c);
                            count++;

                        }
                    }
                   // messageList = getOrganizedList(unOrganizedList);
                    //
                    reader.close();
                } catch (IOException ioe) {

                }


                return unOrganizedList;
            }

        @Override
            protected void onPostExecute(ArrayList<MessagePojo> result) {
            super.onPostExecute(result);
            messageList = getOrganizedList(result);
            messageAdapter = new MessageAdapter(messageList);
            messageRecyclerView.setAdapter(messageAdapter);
            messageAdapter.notifyDataSetChanged();

            int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            Toast.makeText(getContext(),Integer.toString(firstVisibleItem), Toast.LENGTH_SHORT).show();

            dateManipulation();


        }
    }

    MessagePojo pojoFromLine(String k) {
        String text_sender=null, text_time=null, text_message=null, text_date=null; String sub_k=null;
        MessagePojo messagePojo=null;

        try {



            sub_k = k.substring(k.indexOf(':') + 1);

            text_date = k.substring(0,k.indexOf(','));
            text_time = k.substring(k.indexOf(',') + 1, k.indexOf('m') + 1).toUpperCase().trim();
            text_sender = sub_k.substring(sub_k.indexOf('-') + 1, sub_k.indexOf(':')).trim();
            text_message = sub_k.substring(sub_k.indexOf(':') + 1).trim();

            if (text_sender.equals("Richa")) {
                text_sender += " Vats";
            }

            messagePojo = new MessagePojo(text_sender,text_date, text_time, text_message);
            Log.e("ok",k);
        }
        catch (Exception e)
        {
            Log.e("exception",k);
        }
        return messagePojo;


    }

    ArrayList<MessagePojo> getOrganizedList(ArrayList<MessagePojo> list){

        ArrayList<MessagePojo> organizedList = new ArrayList<MessagePojo>();
        String message=null;

        for(int i=0;i<list.size()-1;i++)
        {

                    if(organizedList.size()==0)
                    {
                        organizedList.add(list.get(i));
                    }
                    else {
                        if (list.get(i).getText_sender().equals(organizedList.get(organizedList.size()-1).getText_sender())) {
                            MessagePojo lastOrganizedPojo = organizedList.get(organizedList.size() - 1);
                            message = lastOrganizedPojo.getText_message() + "\n\n" + list.get(i).getText_message();
                            lastOrganizedPojo.setText_message(message);

                        }
                        else {
                            organizedList.add(list.get(i));
                        }
                    }

        }

        return organizedList;

    }

    void dateManipulation() {


        messageRecyclerView.scrollToPosition(0);
        MessagePojo firstVisibleMessage = messageList.get(0);
        int monthCount = Integer.parseInt(firstVisibleMessage.getText_date().substring(3,5));
        if(activeMonth.getText().equals(month[monthCount-1])==false)
            activeMonth.setText(month[monthCount-1]);

        messageRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                        MessagePojo firstVisibleMessage = messageList.get(firstVisibleItem);
                        int monthCount = Integer.parseInt(firstVisibleMessage.getText_date().substring(3,5));
                        if(activeMonth.getText().equals(month[monthCount-1])==false)
                        activeMonth.setText(month[monthCount-1]);
                        //.makeText(getContext(),month[monthCount-1], Toast.LENGTH_SHORT).show();
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        // System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        break;

                }


            }
        });


    }

}
