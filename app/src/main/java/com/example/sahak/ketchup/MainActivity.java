package com.example.sahak.ketchup;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.sahak.ketchup.Fragments.HomeFragment;
import com.example.sahak.ketchup.Fragments.MessageFragment;
import com.example.sahak.ketchup.Fragments.ProfileFragment;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class MainActivity extends AppCompatActivity{


    BottomNavigationViewEx bottomNavigationViewEx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(new MessageFragment());


        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.navigation);
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);

        bottomNavigationViewEx.setIconSize(22, 22);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.action_message:
                        fragment = new MessageFragment();
                        break;

                    case R.id.action_home:
                        fragment = new HomeFragment();
                        break;

                    case R.id.action_profile:
                        fragment = new ProfileFragment();
                        break;
                }


                return loadFragment(fragment);
            }
        });

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
