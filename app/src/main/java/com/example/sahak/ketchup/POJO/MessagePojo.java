package com.example.sahak.ketchup.POJO;

/**
 * Created by sahak on 05-04-2018.
 */

public class MessagePojo {

    String text_sender;
    String text_time;
    String text_message;
    String text_date;


    public MessagePojo(String text_sender,String text_date, String text_time, String text_message) {
        this.text_sender = text_sender;
        this.text_time = text_time;
        this.text_message = text_message;
        this.text_date = text_date;
    }

    public String getText_date() {
        return text_date;
    }

    public void setText_date(String text_date) {
        this.text_date = text_date;
    }

    public String getText_message() {
        return text_message;
    }

    public String getText_sender() {
        return text_sender;
    }

    public String getText_time() {
        return text_time;
    }

    public void setText_message(String text_message) {
        this.text_message = text_message;
    }

    public void setText_sender(String text_sender) {
        this.text_sender = text_sender;
    }

    public void setText_time(String text_time) {
        this.text_time = text_time;
    }
}
